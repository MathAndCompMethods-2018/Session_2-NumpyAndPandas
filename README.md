This Session can be checked out by running the code below from a Jupyter notebook opened within the destination directory:

`! git clone https://git.ecdf.ed.ac.uk/MathAndCompMethods-2018/Session_2-NumpyAndPandas.git`